﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HUD : MonoBehaviour {

    public Sprite[] HealthSprites;
    public Image Heart;
    public Text logged;
    public Text currLevel;
  
    public GameObject tutorialPanel;
    private Player player; // to access health of player



	// Use this for initialization
	void Start () {

        PlayerPrefs.SetString("Level", "SouthKorea");
        

        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        tutorialPanel.SetActive(true);
        logged.text = ("Logged in As: " + PlayerPrefs.GetString("Username"));
        currLevel.text = ("Current Level : " + PlayerPrefs.GetString("Level"));

    }
	
	// Update is called once per frame
	void Update () {
        Heart.sprite = HealthSprites[player.currentHealth];
      
    } 

    public void closePanel()
    {
        player.playSound();
        tutorialPanel.SetActive(false);
    }

   
}
