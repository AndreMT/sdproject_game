﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ChestLoadQuiz : MonoBehaviour {

    public Text load;
 
    void start()
    {
       
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.CompareTag("Player"))
        {
           
            load.text = ("Press [F] to LOAD QUIZ LEVEL");
        }
    }
    void OnTriggerStay2D(Collider2D col)
    {
        if (col.CompareTag("Player"))
        {
            if(Input.GetKeyDown("f"))
            {              
                Application.LoadLevel("FinalQuiz");
            }          
        }
    }
    void OnTriggerExit2D(Collider2D col)
    {
        if (col.CompareTag("Player"))
        {
            load.text = ("");
        }
    }
}
