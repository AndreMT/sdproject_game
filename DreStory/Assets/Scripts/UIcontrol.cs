﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Text;
using System;
using System.Text.RegularExpressions;
using UnityEngine.SceneManagement;

public class UIcontrol : MonoBehaviour {

    // PHP URL VARIABLES
    public string connectionURL;
    public string logURL;
    public string registerURL;
    public string checkUsernameUrl;
    public string checkEmailUrl;


    // UI MULTIPLE CANVAS VARIABLES
    private GameObject playCanvas;
    private GameObject signCanvas;
    private GameObject registerCanvas;
    public AudioClip clip;
   

    // REGISTER INPUT FIELDS VARIABLES

   
    public InputField username;
    public InputField password;
    public InputField email;
    public Toggle jill;
    //public Toggle jack;
    public Text msg;

    // SIGN IN VARIABLES 
    public Text loginmsg;
    public InputField loginusername;
    public InputField loginpassword;
   
    private bool jill_bool = false;
   // private bool jack_bool = false;
    private string charSelected;
   
  
   
    void Start() {

        playCanvas = GameObject.Find("HUD").transform.FindChild("Playcanvas").gameObject;
        signCanvas = GameObject.Find("HUD").transform.FindChild("SignInCanvas").gameObject;
        registerCanvas = GameObject.Find("HUD").transform.FindChild("RegisterCanvas").gameObject;
        // Register inputListeners for Toggles  
        jill.onValueChanged.AddListener(delegate { inputListener(); });
      //  jack.onValueChanged.AddListener(delegate { inputListener(); });
        

    }
    public void inputListener()
    {
        // SETTING INPUT VARIABLES ACCORDING TO UI TOGGLES  

        if (jill.isOn)
        {
            jill_bool = true;
            charSelected = "Jill";
        }
        else
        {
            jill_bool = false;
           
        }
        //if (jack.isOn)
        //{
        //    jack_bool = true;
        //    charSelected = "Jack";
        //}
        //else
        //{
        //    jack_bool = false;
        //}

      //  Debug.Log(" " + username_text + " " + password_text + " " + email_text + " " + jill_bool + jack_bool);
    }

    // Update is called once per frame
    void Update() {

      

        if (Input.GetKey(KeyCode.Escape))
        {
            signCanvas.SetActive(false);
            registerCanvas.SetActive(false);
            playCanvas.SetActive(true);
            playSound();
        }

        
        
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void playSound()
    {
        this.gameObject.AddComponent<AudioSource>();
        this.GetComponent<AudioSource>().clip = clip;
        this.GetComponent<AudioSource>().Play();
    }
    public void registerLoad()
    {
        playSound();
        registerCanvas.SetActive(true);
        signCanvas.SetActive(false);
    }
    public void backtoLogin()
    {
        playSound();
        registerCanvas.SetActive(false);
        signCanvas.SetActive(true);
    }
    public void Play()
    {
        playSound();
        playCanvas.SetActive(false);
        signCanvas.SetActive(true);
    }
    public void Register(GameObject registerButton)
    {
        playSound();
        StartCoroutine(RegisterIE());
    }
    public void LoginLoad()
    {
        playSound();
      
        StartCoroutine(LoginIE(loginusername.text, loginpassword.text));

        PlayerPrefs.SetInt("LoggedIn", 1); // IF LOGGED IN ==1 
        PlayerPrefs.SetString("Username", loginusername.text);
        Debug.Log(PlayerPrefs.GetString("Username"));   
            
    }
   
    IEnumerator LoginIE(string user, string pw)
    {
        bool error = false;

        if (!error)
        {
            if (loginusername.text == "")
            {
                loginusername.image.color = Color.red;
                loginmsg.text = "Please Enter Username";
                error = true;
            }
            else
            {
                loginusername.image.color = Color.white;
                error = false;
            }
        }
        if (!error)
        {
            if (loginpassword.text == "")
            {
                loginpassword.image.color = Color.red;
                loginmsg.text = "Please Enter Password";
                error = true;
            }
            else
            {
                loginpassword.image.color = Color.white;
                error = false;
            }
        }
        if (!error)
        {
                WWWForm form = new WWWForm();
                form.AddField("name", user);
                form.AddField("password", pw);

                WWW www = new WWW(logURL, form);
                yield return www;
                string _return = www.text;
     

               if (_return == "false")
                {
                    loginmsg.text = "Incorrect Username or Password";
                    PlayerPrefs.SetInt("LoggedIn", 0); // NOT LOGGED IN 
                }
               
                else if (_return == "true")
                {
                   
                    loginmsg.text = " OK";
                    SceneManager.LoadScene("PlatformOne");

            }
               
        }
       
        
          
      }
    IEnumerator RegisterIE()
    {
        bool error = false;

        if (username.text == "")
        {
            username.image.color = Color.red;
            msg.text = "Please Enter Username";
            error = true;
        }
        else
        {
            //CHECKING IF USERNAME EXISTS ALREADY
            WWWForm form = new WWWForm();
            form.AddField("name", username.text);
            WWW www = new WWW(checkUsernameUrl, form);
            yield return www;

         
            string _return = www.text;
            if (_return == "false")
            {
                username.image.color = Color.red;
                msg.text = "Username already exists, please choose another";
                error = true;
            }
            //BELOW CODE TO TEST IF checkUsernameURL WORKING WITH PHP AND UNITY:
            else if (_return == "true")
            {
                username.image.color = Color.white;
                
            }        
        }
        if (!error)
        {
            if (password.text == "")
            {
                password.image.color = Color.red;
                msg.text = "Please Enter Password";
                error = true;
            }  
            else
            {
                password.image.color = Color.white;         
            }               
        }
        if (!error)
        {
            if (email.text == "")
            {
                msg.text = "Please Enter E-mail";
                email.image.color = Color.red;
                error = true;
            }
            else if (isEmailValid(email.text) == false)
            {
                msg.text = "Please Enter a CORRECT E-mail address";
                email.image.color = Color.red;
                error = true;
            }
            else
            {
                WWWForm form = new WWWForm();
                form.AddField("email", email.text);

                WWW www = new WWW(checkEmailUrl, form);
                yield return www;
                string _return = www.text;
                if (_return == "false")
                {
                    email.image.color = Color.red;
                    msg.text = "Email Already Exists";
                    error = true;
                }
                else if (_return == "true")
                {
                    email.image.color = Color.white;
                }
            }
        }   
        if (!error)
          {
            //if (jack.isOn && jill.isOn || jill.isOn == false && jack.isOn == false )
            if(jill.isOn == false)
            {
                msg.text = "Please Choose ONE Character! :)";
              //  jack.image.color = Color.red;
                jill.image.color = Color.red;
              //  jack.isOn = false;
                jill.isOn = false;
                error = true;

            }
            else 
            {               
              //  jack.image.color = Color.white;
                jill.image.color = Color.white;
            } 
          }         
          if (!error)
          {
            WWWForm form = new WWWForm();
            form.AddField("name", username.text);
            form.AddField("password", Md5Sum(password.text));
            form.AddField("email", email.text);
            form.AddField("charselected", charSelected);

     
            WWW www = new WWW(registerURL, form);
            yield return www;
            Debug.Log(www.text);
            string _return = www.text;
            if (_return == "true")
            {
                msg.text = "Registration Done!";
                yield return new WaitForSeconds(2);                      
            }
            else
            {
                msg.text = "Server offline";
                error = true;
            }
        }      
    }

       
  public bool isEmailValid (string email)
    {
        try
        {
            Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
            Match match = regex.Match(email);
            if (match.Success)
            {
                Debug.Log("correct");
                return true;
            }
            else
            {
                Debug.Log("Incorrect");
                return false;
            }
               
        }
        catch (FormatException)
        {
            Debug.Log(" fail");
            return false;
        }

    }

    public string Md5Sum(string _data)
    {
        // step 1, calculate MD5 hash from input
        System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create();
        byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(_data);
        byte[] hash = md5.ComputeHash(inputBytes);

        // step 2, convert byte array to hex string
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < hash.Length; i++)
        {
            sb.Append(hash[i].ToString("X2"));
        }
        return sb.ToString();
    }

}
