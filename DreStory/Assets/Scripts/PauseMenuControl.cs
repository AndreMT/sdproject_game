﻿using UnityEngine;
using System.Collections;

public class PauseMenuControl : MonoBehaviour {

    public GameObject menu;
    public AudioClip clip;

    private bool paused = false;

	// Use this for initialization
	void Start () {
        menu.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
	
        if(Input.GetButtonDown("Pause"))
        {
            playSound();
            paused = !paused;
        }
        if (paused)
        {
            menu.SetActive(true);
            Time.timeScale = 0; // pauses the actual game.
        }
        if (!paused)
        {
            
            menu.SetActive(false);
            Time.timeScale = 1; // resuming game//  etc info: slow motion = 0.3
        }
	}
    public void resume()
    {
        playSound();
        paused = false;
    }
    public void restart()
    {
        playSound();
        Application.LoadLevel(Application.loadedLevel);
    }
    public void backtoMenu()
    {
        playSound();
        Application.LoadLevel("MainMenu");
    }
    public void quit()
    {
        playSound();
        Application.Quit();
    }
    public void playSound()
    {
        this.gameObject.AddComponent<AudioSource>();
        this.GetComponent<AudioSource>().clip = clip;
        this.GetComponent<AudioSource>().Play();
    }
}
