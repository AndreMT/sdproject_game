﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.UI;

public class QuizManager : MonoBehaviour {

    public Text question;
    public Text pointsText;
    public Text Logged;
    public Button[] answerButtons;

    public string savePointsUrl;
    public string getAccountIDUrl;
    public string getLevelIDUrl;
    public string checkHighscoresUrl;
    public string updateHighscoresUrl;

    public GameObject[] panels;
    public Text[] scores;
    private int points;


    private int questionCount;
    private int AccountID;
    private int LevelID;
    private int overallscore;

    string[] questions = new string[]
    {
        "When is Cherry Blossom season in South Korea?",
        "What is South Korea's Offical Language?",
        "Roughly how many people speak Korean worldwide?",
        "Which number is considered bad luck in Korea?",
        "Roughly how many types of Kimchi are there?"
    };

    string[,] answers = new string[,]
    {
        { "Spring","Summer","Autumn","Winter" },
        { "Maltese","Japanese","Chinese","Korean" },
        { "20 million","80 million","1 billion","5 million" },
        { "10","40","4","30" },
        { "100","250","300","500" }
    };

    string[] correctAnswers = new string[]
    {
        "Spring","Korean","80 million","4","250"
    };

    void Start()
    {
        Logged.text = ("Logged In As: " + PlayerPrefs.GetString("Username"));
        panels[2].SetActive(false);
        questionCount = 0;
        points = 0;
        pointsText.text = ("Quiz Points: " + points.ToString());

        question.text = questions[questionCount];

        for (int i = 0; i < 4; i++)
            answerButtons[i].GetComponentInChildren<Text>().text = answers[questionCount, i];
    }

    public void ClickAnswerOne()
    {
        CheckAnswer(questionCount, answerButtons[0].GetComponentInChildren<Text>().text);
        NextQuestion();
    }

    public void ClickAnswerTwo()
    {
        CheckAnswer(questionCount, answerButtons[1].GetComponentInChildren<Text>().text);
        NextQuestion();
    }

    public void ClickAnswerThree()
    {
        CheckAnswer(questionCount, answerButtons[2].GetComponentInChildren<Text>().text);
        NextQuestion();
    }

    public void ClickAnswerFour()
    {
        CheckAnswer(questionCount, answerButtons[3].GetComponentInChildren<Text>().text);
        NextQuestion();
    }

    void CheckAnswer(int questionNum, string answer)
    {
        // Debug.Log(correctAnswers[questionNum]);

        if (correctAnswers[questionNum] == answer)
        {
            points += 10;
        }
    }

    void NextQuestion()
    {
        pointsText.text = ("Quiz Points:" + points.ToString());
        questionCount++;

        if (questionCount == questions.Length)
        {
            FinishGame();
        }
        else
        {
            question.text = questions[questionCount];
            for (int i = 0; i < 4; i++)
                answerButtons[i].GetComponentInChildren<Text>().text = answers[questionCount, i];
        }
    }

    void FinishGame()
    {
        panels[0].SetActive(false);
        panels[1].SetActive(false);

        scores[0].text = ("Score Overview: " + PlayerPrefs.GetString("Level"));
        scores[1].text = ("Platform Points: " + PlayerPrefs.GetInt("Score").ToString());
        scores[2].text = ("Quiz Points: " + points);
        scores[3].text = ("OVERALL SCORE: " + FinalScore());
        panels[2].SetActive(true);



        StartCoroutine(savePointsIE());



    }
    string FinalScore()
    {
        overallscore = points + PlayerPrefs.GetInt("Score");
        return overallscore.ToString();
    }

    public void restartLevl()
    {
        PlayerPrefs.SetString("Level", "");
        PlayerPrefs.SetInt("Score", 0);
        Application.LoadLevel("PlatformOne");
    }
    public void Logout()
    {
        PlayerPrefs.SetInt("LoggedIn", 0);
        PlayerPrefs.SetString("Username", "");
        PlayerPrefs.SetString("Level", "");
        PlayerPrefs.SetInt("Score", 0);
        Application.LoadLevel("MainMenu");
    }
    IEnumerator savePointsIE()
    {
        bool error = false;

        if (!error)
        {
            WWWForm form = new WWWForm();
            form.AddField("username", PlayerPrefs.GetString("Username"));

            WWW www = new WWW(getAccountIDUrl, form);
            yield return www;
            string e = www.text;
            AccountID = int.Parse(e);
            Debug.Log(AccountID);
            if (www.text == "")
            {
                Debug.Log("empty");
                error = true;
            }
            else
            {
                WWWForm form1 = new WWWForm();
                form1.AddField("levelname", PlayerPrefs.GetString("Level"));

                WWW www1 = new WWW(getLevelIDUrl, form1);
                yield return www1;
                string ee = www1.text;
                LevelID = int.Parse(ee);
                Debug.Log(LevelID);
                if (www1.text == "")
                {
                    Debug.Log("empty");
                    error = true;
                }
                else
                {
                    ee.ToString();
                }
            }
        }
        if (!error)
        {                       
                WWWForm form = new WWWForm();
                form.AddField("username", PlayerPrefs.GetString("Username"));

                WWW www = new WWW(checkHighscoresUrl, form);
                yield return www;

                if (www.text == "true")
                {
                // update table
                
                    Debug.Log("Already Played Before - Updating Table");
                    WWWForm form4 = new WWWForm();
                    Debug.Log(AccountID);
                    form4.AddField("accountID", AccountID);
                    form4.AddField("levelID", LevelID);
                    form4.AddField("pointsPlatform", PlayerPrefs.GetInt("Score"));
                    form4.AddField("pointsQuiz", points);
                    form4.AddField("totalScore", overallscore);
                    

                    WWW www3 = new WWW(updateHighscoresUrl, form4);
                    yield return www3;
                    string e = www3.text;
                    Debug.Log(e);
                    if (www3.text == "false")
                    {                     
                        Debug.Log(" not inserted");
                    }
                    else
                    {

                        Debug.Log("Points Saved Successfully" + e);
                    }

            }

                else if (www.text =="false")
                {
                    // insert new table row
                    
                    Debug.Log("New Player");

                    WWWForm form3 = new WWWForm();
                    form3.AddField("accountID", AccountID);
                    form3.AddField("levelID", LevelID);
                    form3.AddField("pointsPlatform", PlayerPrefs.GetInt("Score"));
                    form3.AddField("pointsQuiz", points);
                    form3.AddField("totalScore", overallscore);

                    WWW www3 = new WWW(savePointsUrl, form3);
                    yield return www3;
                    string e = www3.text;
                    Debug.Log(e);
                    if (www3.text == "false")
                    {
                        error = true;
                        Debug.Log(" not inserted");
                    }
                    else
                    {

                        Debug.Log("Points Saved Successfully" + e);
                    }
                }
                else
                {
                    Debug.Log("Error inserting highscores");
                }

           }

        }
    }


   
