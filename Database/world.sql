-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 04, 2017 at 08:25 PM
-- Server version: 10.1.22-MariaDB
-- PHP Version: 7.1.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `world`
--

-- --------------------------------------------------------

--
-- Table structure for table `acclevel`
--

CREATE TABLE `acclevel` (
  `userlevelID` int(11) NOT NULL,
  `accountID` int(11) DEFAULT NULL,
  `levelID` int(11) DEFAULT NULL,
  `pointsPlatform` int(11) DEFAULT NULL,
  `pointsQuiz` int(11) DEFAULT NULL,
  `totalscore` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `acclevel`
--

INSERT INTO `acclevel` (`userlevelID`, `accountID`, `levelID`, `pointsPlatform`, `pointsQuiz`, `totalscore`) VALUES
(7, 45, 1, 250, 50, 300),
(9, 47, 1, 245, 10, 255),
(13, 49, 1, 245, 40, 285),
(15, 50, 1, 205, 50, 255),
(16, 51, 1, 250, 40, 290),
(17, 54, 1, 220, 30, 250),
(18, 55, 1, 240, 30, 270),
(19, 56, 1, 205, 40, 245),
(20, 57, 1, 275, 50, 325),
(21, 58, 1, 250, 20, 270),
(22, 60, 1, 230, 10, 240),
(23, 61, 1, 230, 10, 240);

-- --------------------------------------------------------

--
-- Table structure for table `account`
--

CREATE TABLE `account` (
  `accountID` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `charSelected` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `account`
--

INSERT INTO `account` (`accountID`, `username`, `password`, `email`, `charSelected`) VALUES
(34, 'test', '098F6BCD4621D373CADE4E832627B4F6', 'test', 'Jill'),
(35, 'test', '098F6BCD4621D373CADE4E832627B4F6', 'test', 'Jill'),
(36, '12', 'C20AD4D76FE97759AA27A0C99BFF6710', '12', 'Jill'),
(37, '123', '202CB962AC59075B964B07152D234B70', '124', 'Jack'),
(38, '13', 'C51CE410C124A10E0DB5E4B97FC2AF39', '13', 'Jill'),
(39, '113', '73278A4A86960EEB576A8FD4C9EC6997', '113', 'Jack'),
(40, '3', 'C20AD4D76FE97759AA27A0C99BFF6710', '3', 'Jill'),
(41, '5', '202CB962AC59075B964B07152D234B70', '1234', 'Jack'),
(42, '12345', '827CCB0EEA8A706C4C34A16891F84E7B', '12345', 'Jack'),
(43, 'testing', '098F6BCD4621D373CADE4E832627B4F6', '123456', 'Jack'),
(44, 'charmi', '202CB962AC59075B964B07152D234B70', 'charmejn98@gmail.com', 'Jill'),
(45, 'lilly', '81DC9BDB52D04DC20036DBD8313ED055', '1234567', 'Jill'),
(47, 'Andre', 'E10ADC3949BA59ABBE56E057F20F883E', 'ylemar@live.inf', 'Jill'),
(49, 'AndreMT', '6B5D3C7A5B8FD18E8FEC965630A5A19D', 'andmar97@live.com', 'Jill'),
(50, 'May', '81DC9BDB52D04DC20036DBD8313ED055', '23@live.com', 'Jill'),
(51, 'mary', 'B00A50C448238A71ED479F81FA4D9066', 'marymarmara@live.com', 'Jill'),
(52, 'mary1', 'B00A50C448238A71ED479F81FA4D9066', 'marymarmara1@live.com', 'Jill'),
(54, 'Charmaine', '90D691888091ED98126A8796603050AC', 'charmaine982@gmail.com', 'Jill'),
(55, 'JurgenC', '92B420A596DAC7F950D12AAAFB65E155', 'jurgenC@gmail.com', 'Jill'),
(56, 'Maya', '81DC9BDB52D04DC20036DBD8313ED055', 'attardmaya@hotmail.com', 'Jill'),
(57, 'FinalTest', '81DC9BDB52D04DC20036DBD8313ED055', 'testing@gaul.com', 'Jill'),
(58, 'Register', '202CB962AC59075B964B07152D234B70', 'regiuster@google.com', 'Jill'),
(59, 'Register1', '3D9188577CC9BFE9291AC66B5CC872B7', 'reg1@google.com', 'Jill'),
(60, 'Register2', 'E10ADC3949BA59ABBE56E057F20F883E', 'reg21@google.com', 'Jill'),
(61, 'UPDATETEST', '202CB962AC59075B964B07152D234B70', 'ylemar@sdjkhfkjsd.com', 'Jill');

-- --------------------------------------------------------

--
-- Table structure for table `levels`
--

CREATE TABLE `levels` (
  `LevelID` int(11) NOT NULL,
  `levelname` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `levels`
--

INSERT INTO `levels` (`LevelID`, `levelname`) VALUES
(1, 'SouthKorea');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `acclevel`
--
ALTER TABLE `acclevel`
  ADD PRIMARY KEY (`userlevelID`),
  ADD KEY `accountID` (`accountID`),
  ADD KEY `levelID` (`levelID`);

--
-- Indexes for table `account`
--
ALTER TABLE `account`
  ADD PRIMARY KEY (`accountID`);

--
-- Indexes for table `levels`
--
ALTER TABLE `levels`
  ADD PRIMARY KEY (`LevelID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `acclevel`
--
ALTER TABLE `acclevel`
  MODIFY `userlevelID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `account`
--
ALTER TABLE `account`
  MODIFY `accountID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;
--
-- AUTO_INCREMENT for table `levels`
--
ALTER TABLE `levels`
  MODIFY `LevelID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `acclevel`
--
ALTER TABLE `acclevel`
  ADD CONSTRAINT `acclevel_ibfk_1` FOREIGN KEY (`accountID`) REFERENCES `account` (`accountID`),
  ADD CONSTRAINT `acclevel_ibfk_2` FOREIGN KEY (`levelID`) REFERENCES `levels` (`LevelID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
